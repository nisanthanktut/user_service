package com.userservice.UserService.controllers;

import com.userservice.UserService.dtos.UserDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/api/user")
public class UserController {

    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return Arrays.asList(
                new UserDTO("1", "Thushan", "24"),
                new UserDTO("2", "Nishani", "22")
        );
    }
}
